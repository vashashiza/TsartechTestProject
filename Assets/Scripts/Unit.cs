﻿using System;
using UnityEngine;

public class Unit : DisintegratableUnit, IPoolable
{
    public event Action OnPositionChange;
    public event Action OnDisintegrate;

    [SerializeField]
    private float _moveSpeed;
    /// <summary>
    /// game object's speed; can be set from outside;
    /// </summary>
    public float MoveSpeed
    {
        set
        {
            _moveSpeed = value;
        }
    }

    private bool _moveActive = false;
    /// <summary>
    /// Is game object able to move; can be set from outside;
    /// </summary>
    public bool MoveActive
    {
        set
        {
            _moveActive = value;
        }
    }

    public override void Disintegrate()
    {
        _moveActive = false;
        RaiseOnDisintegrate();
        base.Disintegrate();
    }

    private void RaiseOnPositionChange()
    {
        if (OnPositionChange != null) OnPositionChange();
    }

    private void RaiseOnDisintegrate()
    {
        if (OnDisintegrate != null) OnDisintegrate();
    }

    /// <summary>
    /// determine game object's movement
    /// </summary>
    /// <param name="target"> movement target position</param>
    private void Move(Vector3 target)
    {
        Vector3 moveSpeedVector3 = target * (_moveSpeed * Time.deltaTime);
        transform.position += moveSpeedVector3;
        RaiseOnPositionChange();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Plane")
        {
            Disintegrate();
        }
    }

    private void Update()
    {
        if (_moveActive)
        {
            Move(Vector3.forward);
        }
    }
}
