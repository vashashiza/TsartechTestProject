﻿using UnityEngine;

public class DisintegratableUnit : MonoBehaviour, IPoolable
{
    /// <summary>
    /// unique token for get object from pool
    /// </summary>
    [SerializeField]
    private UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }
    }

    void IPoolable.Off()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// destroying this game object
    /// </summary>
    public virtual void Disintegrate()
    {
        if ((object)this != null)
        {
            Pool.Instance.Add(this);
        }
    }
}