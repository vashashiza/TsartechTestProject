﻿using UnityEngine;
using UnityEngine.UI;

public class UnitTextField : DisintegratableUnit, IPoolable
{
    [SerializeField]
    private float heightToTarget = 1.8f;

    /// <summary>
    /// ratio of target size to this UI element
    /// </summary>
    [SerializeField]
    private Vector3 ratio = new Vector3();

    /// <summary>
    /// reduction factor of this object
    /// </summary>
    private float reductionFactor;

    /// <summary>
    /// first scale of this object
    /// </summary>
    [SerializeField]
    private float baseScale;

    /// <summary>
    /// target with which this object moves
    /// </summary>
    [SerializeField]
    private Unit target;
    public Unit Target
    {
        set
        {
            target = value;
        }
    }

    [SerializeField]
    private Text txt = null;
    public Text Txt
    {
        get
        {
            return txt;
        }

        set
        {
            txt = value;
        }
    }


    public override void Disintegrate()
    {
        target.OnPositionChange -= Target_OnPositionChange;
        target.OnDisintegrate -= Target_OnDisintegrate;

        base.Disintegrate();
    }
    private void Target_OnDisintegrate()
    {
        Disintegrate();
    }

    private void Target_OnPositionChange()
    {
        ChangePosition();
    }

    private void ChangePosition()
    {
        if (target == null)
        {
            Disintegrate();
            return;
        }

        CalculatePositionAndSize();
    }

    private void CalculatePositionAndSize()
    {
        Txt.transform.localScale = ratio / Vector3.Distance(Camera.main.transform.position, target.transform.position);

        reductionFactor = baseScale / Txt.transform.localScale.y;

        Vector3 temp = new Vector3(target.transform.position.x, target.transform.position.y + target.transform.localScale.y / 2, target.transform.position.z);

        Vector3 txtWorldPosition = temp + new Vector3(0.0f, heightToTarget / reductionFactor, 0.0f);
        transform.position = Camera.main.WorldToScreenPoint(txtWorldPosition);
    }

    private void OnEnable()
    {        
        target.OnPositionChange += Target_OnPositionChange;
        target.OnDisintegrate += Target_OnDisintegrate;

        CalculatePositionAndSize();
    }


#if UNITY_EDITOR
    [ContextMenu("SetDeltaSize")]
    public void SetDeltaSize()
    {
        float distance = Vector3.Distance(Camera.main.transform.position, target.transform.position);
        ratio = Txt.transform.localScale * distance;
        baseScale = Txt.transform.localScale.y;
    }
#endif
}
