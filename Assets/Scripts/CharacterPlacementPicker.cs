﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterPlacementPicker : MonoBehaviour, IPointerDownHandler, IEndDragHandler, IDragHandler
{
    public event Action<Vector3, UnitType> OnDropped;

    [SerializeField]
    private UnitType type;
    [SerializeField]
    private Transform iconParent = null;

    //instantiated object of a class Icon
    private DisintegratableUnit iconObject = null;

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        InstantiateIcon(eventData.pressPosition);
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        iconObject.transform.position = Input.mousePosition;
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if (iconObject == null)
        {
            return;
        }

        iconObject.Disintegrate();
        iconObject = null;

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        Vector3 position;
        if (IsPlane(out position))
        {
            RaiseOnDropped(position);
        }
    }

    private bool IsPlane( out Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (!Physics.Raycast(ray, out hitInfo))
        {
            position = Vector3.zero;
            return false;
        }

        if (hitInfo.collider.tag != "Plane")
        {
            position = Vector3.zero;
            return false;
        }

        position = hitInfo.point;

        return true;
    }

    private void RaiseOnDropped(Vector3 pos)
    {
        if (OnDropped != null) OnDropped(pos, type);
    }

    private void InstantiateIcon(Vector3 pos)
    {
        if (iconObject != null)
        {
            return;
        }

        iconObject = Pool.Instance.Get<DisintegratableUnit>(type);
        iconObject.transform.SetParent(iconParent);
        iconObject.transform.position = pos;
        iconObject.gameObject.SetActive(true);
    }
}
