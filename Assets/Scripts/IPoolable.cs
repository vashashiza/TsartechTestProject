﻿/// <summary>
/// For objects are able to add to the Poll
/// </summary>
public interface IPoolable
{
    /// <summary>
    /// object's type
    /// </summary>
    UnitType Type { get; }

    /// <summary>
    /// Makes the object inaccessible to interaction.
    /// </summary>
    void Off();
}

/// <summary>
/// object's types for objects one class
/// </summary>
public enum UnitType
{
    GreenUnit = 0,
    RedUnit = 1,
    BlueUnit = 2,
    GreenIcon = 3,
    RedIcon = 4,
    BlueIcon = 5,
    Txt = 6
}