﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Serializable] public class IconUnitPair : SerializableKeyValuePair<UnitType, UnitType> { }
    [Serializable] public class UnitCountPair : SerializableKeyValuePair<UnitType, int> { }

    [SerializeField]
    private List<IconUnitPair> iconUnitPairs = new List<IconUnitPair>();
    [SerializeField]
    private List<CharacterPlacementPicker> characterPlacementPickers = new List<CharacterPlacementPicker>();

    private List<UnitCountPair> unitCountPairs = new List<UnitCountPair>();

    [SerializeField]
    private int minUnitSpeed = 1;
    [SerializeField]
    private int maxUnitSpeed = 10;

    [SerializeField]
    private Transform txtFieldsParent = null;


    private void Item_OnDropped(Vector3 pos, UnitType type)
    {
        int number = CalculateInstantiatedModels(type);
        Unit unit = InstantiateModel(pos, type);

        InstantiateTextField(unit, number);        
    }

    private void SetMoveSpeed(Unit unit)
    {
        int speed = UnityEngine.Random.Range(minUnitSpeed, maxUnitSpeed + 1);
        unit.MoveSpeed = speed;
    }

    private int CalculateInstantiatedModels(UnitType type)
    {
        int i = unitCountPairs.FindIndex(x => x.Key == type);

        if (i == -1)
        {
            UnitCountPair pair = new UnitCountPair();
            pair.Key = type;
            pair.Value = 1;
            unitCountPairs.Add(pair);
            return 1;
        }

        unitCountPairs[i].Value++;

        return unitCountPairs[i].Value;
    }

    private Unit InstantiateModel(Vector3 pos, UnitType type)
    {
        int i = iconUnitPairs.FindIndex(x => x.Key == type);

        if (i == -1)
        {
            Debug.LogError("item doesn't found");
            return null;
        }

        Unit unit = Pool.Instance.Get<Unit>(iconUnitPairs[i].Value);
        unit.transform.position = new Vector3(pos.x, unit.transform.position.y, pos.z);
        unit.gameObject.SetActive(true);

        SetMoveSpeed(unit);
        unit.MoveActive = true;

        return unit;
    }

    private void InstantiateTextField(Unit unit, int number)
    {
        UnitTextField txt = Pool.Instance.Get<UnitTextField>(UnitType.Txt);
        txt.transform.SetParent(txtFieldsParent);
        txt.Target = unit;
        txt.Txt.text = number+"";
        txt.gameObject.SetActive(true);
    }

    private void Awake()
    {
        foreach (CharacterPlacementPicker item in characterPlacementPickers)
        {
            item.OnDropped += Item_OnDropped;
        }
    }
}

